﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace n01289618BonusAssignment1
{
    public partial class CartesianSmartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //Cartesian Smartesian
        protected void CartesianSmartesianResult(object sender, EventArgs e)
        {
            //Set new instantiation of SmartesianCartesian's xAxisValue & yAxisValue to form's TextBox input
            int xAxisValue = int.Parse(xAxis.Text);
            int yAxisValue = int.Parse(yAxis.Text);

            //Instantiate new class 
            CartesianPlane SmartesianCartesian = new CartesianPlane(xAxisValue, yAxisValue);
            
            //Change div html to output our answer
            quadrantResult.InnerHtml = SmartesianCartesian.quadrantResultCalculation();
        }

        //Divisible Sizzable
        protected void DivisibleSizzable(object sender, EventArgs e)
        {
            //Set number to hold number from input box
            int numberToTest = int.Parse(firstNumber.Text);

            //Set class number to input box number
            DivisibleSizzable PrimeNumber = new DivisibleSizzable(numberToTest);

            //Perform class method primeResultCalculations
            primeResult.InnerHtml = PrimeNumber.primeResultCalculation();
        }

        //String Bling
        protected void StringBling(object sender, EventArgs e)
        {
            //Set string to input box string
            string wordToTest = inputString.Text;

            //Set class string to input box string
            StringBling PalindromeCheck = new StringBling(wordToTest);

            //Run class method isPalindrome 
            palindromeResult.InnerHtml = PalindromeCheck.isPalindrome();
        }

    }
}