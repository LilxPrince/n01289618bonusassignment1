﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace n01289618BonusAssignment1
{
    public class StringBling
    {
        public string word;

        public StringBling(string w)
        {
            word = w;
        }

        public string isPalindrome()
        {
            //Instantiate string to blank
            string palindromeOutput = "";

            //Transform string to lowercase
            word.ToLower();

            //Remove string's spaces
            word.Replace(" ", string.Empty);

            //Had trouble working with string indexs as specified in the pdf
            //Ended up using char
            //Made a char array with string converted to a char
            char[] reverseWord = word.ToCharArray();

            //Take the array of the word now and reverse it
            Array.Reverse(reverseWord);

            //Now convert it back to a string
            string wordReverse = new string(reverseWord);

            //Do string comparisons with if statement
            if(wordReverse == word)
            {
                palindromeOutput = "<br/> The word IS a palindrome";
            }
            else
            {
                palindromeOutput = "<br/> The word is NOT a palindrome";
            }

            return palindromeOutput;
        }
    }
}