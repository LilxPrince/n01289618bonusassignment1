﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace n01289618BonusAssignment1
{
    public class DivisibleSizzable
    {
        //Declare variable to work with
        public int number;

        public DivisibleSizzable(int i)
        {
            //Set class variable to parameter variable
            number = i;
        }

        //https://en.wikipedia.org/wiki/Primality_test
        //http://mathforum.org/dr.math/faq/faq.prime.num.html
        public string primeResultCalculation()
        {
            string isPrime = "<br/> The number IS a prime number";

            //1 and anything under it is not prime
            if(number <= 1)
            {
                isPrime = "<br/> The number is NOT a prime number";
            }

            //2 is a prime number
            else if (number == 2)
            {
                isPrime = "<br/> The number IS a prime number";
            }

            //If the number is even / can be divided by 2 then it is not prime
            else if(number % 2 == 0)
            {
                isPrime = "<br/> The number is NOT a prime number";
            }

            //Largest factor of division
            int largestFactor = (int)Math.Floor(Math.Sqrt(number));

            //To test primality we must divide all integer divisors of the number
            //The boundary for this is the square root of the number, we will never have to divide numbers higher than the numbers square root
            //We divide by odd numbers bc we already know that % 2 == 0 is not prime numbers
            for(int i = 3; i <= largestFactor; i+=2)
            {
                if (number % i == 0)
                {
                    isPrime = "<br/> The number is NOT a prime number";
                }

            }

            return isPrime;
        }



    }
}