﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BonusAssignment.aspx.cs" Inherits="n01289618BonusAssignment1.CartesianSmartesian" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>HTTP5101B Bonus Assignment 1</title>
</head>
<body>
    <form id="form1" runat="server">
        <%--Cartesian Smartesian--%>
        <div>
            <h1>Cartesian Smartesian</h1>
            <%--X axis--%> 
            <asp:TextBox runat="server" ID="xAxis" placeholder="X-Value"></asp:TextBox>
            <%--<asp:RequiredFieldValidator runat="server" ID="xAxisRequired" ControlToValidate="xAxis" ForeColor="Red" ErrorMessage="You must enter a X value"></asp:RequiredFieldValidator>--%>
            <asp:CompareValidator runat="server" ID="compareX" ControlToValidate="xAxis" ForeColor="Red" ErrorMessage="Cannot be 0" Operator="NotEqual" ValueToCompare="0"></asp:CompareValidator>
            
            <%--Y axis--%>
            <asp:TextBox runat="server" ID="yAxis" placeholder="Y-Axis"></asp:TextBox>
            <%--<asp:RequiredFieldValidator runat="server" ID="yAxisRequired" ControlToValidate="yAxis" ForeColor="Red" ErrorMessage="You must enter a Y value"></asp:RequiredFieldValidator>--%>
            <asp:CompareValidator runat="server" ID="compareY" ControlToValidate="yAxis" ForeColor="Red" ErrorMessage="Cannot be 0" Operator="NotEqual" ValueToCompare="0"></asp:CompareValidator>
        </div>

    <%--Button to run C# for Cartesian Smartesian--%>
    <br />
    <br />
    <asp:Button runat="server" ID="button" OnClick="CartesianSmartesianResult" Text="Submit Coordinates" />
    <%--Result output for Cartesian Smartesian--%>
    <div id="quadrantResult" runat="server"></div>

        <%--Divisible Sizzable--%>
        <div>
            <h1>Divisible Sizzable</h1>
            <%--Number--%>
            <asp:TextBox runat="server" ID="firstNumber" placeholder="Number one"></asp:TextBox>
            <%--<asp:RequiredFieldValidator runat="server" ID="firstNumberRequired" ControlToValidate="firstNumber" ForeColor="Red" ErrorMessage="You must enter a value"></asp:RequiredFieldValidator>--%>
            <asp:RangeValidator ID="firstNumberValidation" runat="server" ForeColor="Red" ErrorMessage="Must be positive number" ControlToValidate="firstNumber" MinimumValue="0" MaximumValue="999"></asp:RangeValidator>

        </div>
        <%--Button to run C# for Divisible Sizzable--%>
        <br />
        <br />
        <asp:Button runat="server" ID="button1" OnClick="DivisibleSizzable" Text="Submit Number" />
        <%--Result output for Divisible Sizzable--%>
        <div id="primeResult" runat="server"></div>

        <%--String Bling--%>
        <div>
            <h1>String Bling</h1>
            <%--Input String--%> 
            <asp:TextBox runat="server" ID="inputString" placeholder="Enter a word"></asp:TextBox>
            <%--<asp:RequiredFieldValidator runat="server" ID="inputStringRequired" ForeColor="Red" ControlToValidate="inputString" ErrorMessage="You must enter a word"></asp:RequiredFieldValidator>--%>
            <asp:RegularExpressionValidator runat="server" id="validateInputString" ControlToValidate="inputString" ValidationExpression="^[a-zA-Z]+$" ErrorMessage="Please enter a valid word"></asp:RegularExpressionValidator>
        </div>
        <%--Button to run C# for String Bling--%>
        <br />
        <br />
        <asp:Button runat="server" ID="button2" OnClick="StringBling" Text="Submit Word" />
        <%--Result output for String Bling--%>
        <div id="palindromeResult" runat="server"></div>
    </form>
</body>
</html>
