﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace n01289618BonusAssignment1
{
    public class CartesianPlane
    {
        //Declare variables
        public int xAxis;
        public int yAxis;

        public CartesianPlane(int x, int y)
        {
            //Set variables to parameters, get parameter value from form 
            xAxis = x;
            yAxis = y;
        }

        public string quadrantResultCalculation()
        {
            //Output string
            string quadrantOutput = "";

            //If statements for quadrant check
            if(xAxis > 0 && yAxis > 0)
            {
                quadrantOutput = "<br/> Result lies in the first quadrant";
            }

            if(xAxis < 0 && yAxis > 0)
            {
                quadrantOutput = "<br/> Result lies in second quadrant";
            }

            if(xAxis < 0 && yAxis < 0)
            {
                quadrantOutput = "<br/> Result lies in third quadrant";
            }

            if(xAxis > 0 && yAxis < 0)
            {
                quadrantOutput = "<br/> Result lies in fourth quadrant";
            }

            return quadrantOutput;
        }

    }

}

